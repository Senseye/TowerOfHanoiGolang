package main

import (
	"reflect"
	"testing"
)

func TestStack_Push(t *testing.T) {
	stack := Stack{}

	stack.Push(1)

	if reflect.DeepEqual(stack.values, []int{1}) == false {
		t.Fail()
	}
}

func TestStack_Peek(t *testing.T) {
	stack := Stack{}

	stack.Push(1)
	stack.Push(2)
	stack.Push(3)

	if stack.Peek() != 3 {
		t.Fail()
	}

	if reflect.DeepEqual(stack.values, []int{1, 2, 3}) == false {
		t.Fail()
	}
}

func TestStack_Pop(t *testing.T) {
	stack := Stack{}

	stack.Push(1)
	stack.Push(2)
	stack.Push(3)

	if stack.Pop() != 3 {
		t.Fail()
	}

	if stack.Len() != 2 {
		t.Fail()
	}

	values := []int{}

	for _, value := range stack.Iterator() {
		values = append(values, value)
	}

	if reflect.DeepEqual(values, []int{1, 2}) == false {
		t.Fail()
	}
}
