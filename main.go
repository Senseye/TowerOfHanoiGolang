package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

var towerOrder = &[4]string{"A", "B", "C", "D"}

type TowerInterface interface {
	Empty() bool
	Hash() string
	Peek() int
	Push(disk int)
	Pop() int
	Clone() TowerInterface
	Len() int
}

type Tower struct {
	stack *Stack
	hash  string
}

func (t *Tower) Empty() bool {
	return t.stack.Len() == 0
}

func (t *Tower) Hash() string {
	if len(t.hash) > 0 {
		return t.hash
	}

	for _, disk := range t.stack.Iterator() {
		t.hash += strconv.Itoa(disk) + ","
	}

	return t.hash
}

func (t *Tower) Peek() int {
	return t.stack.Peek()
}

func (t *Tower) Push(disk int) {
	t.stack.Push(disk)
	t.hash = ""
}

func (t *Tower) Pop() int {
	t.hash = ""
	return t.stack.Pop()
}

func (t *Tower) Clone() TowerInterface {
	return &Tower{stack: t.stack.Clone()}
}

func (t *Tower) Len() int {
	return t.stack.Len()
}

func NewTower(disks []int) TowerInterface {
	return &Tower{stack: NewStackBySlice(disks)}
}

type StateInterface interface {
	Hash() string
	CanMoveBetween(from, to string) bool
	Move(from, to string) int
	Clone() StateInterface
	TowerMap() map[string]TowerInterface
}

type State struct {
	towerMap map[string]TowerInterface
	hash     string
}

func (s *State) Hash() string {
	if len(s.hash) > 0 {
		return s.hash
	}

	for _, name := range towerOrder {
		if tower, ok := s.towerMap[name]; ok {
			s.hash += name + ":" + tower.Hash() + ";"

		}
	}

	return s.hash
}

func (s *State) CanMoveBetween(from, to string) bool {
	fromTower, _ := s.towerMap[from]

	if fromTower.Empty() {
		return false
	}

	toTower, _ := s.towerMap[to]

	return toTower.Empty() || toTower.Peek() > fromTower.Peek()
}

func (s *State) Move(from, to string) int {
	fromTower, _ := s.towerMap[from]
	toTower, _ := s.towerMap[to]
	disk := fromTower.Pop()
	toTower.Push(disk)
	s.hash = ""
	return disk
}

func (s *State) Clone() StateInterface {
	towerMap := map[string]TowerInterface{}

	for _, name := range towerOrder {
		tower, _ := s.towerMap[name]
		towerMap[name] = tower.Clone()
	}

	return &State{towerMap: towerMap}
}

func (s *State) TowerMap() map[string]TowerInterface {
	return s.towerMap
}

type FreezeState struct {
	StateInterface
}

func (f *FreezeState) Move(from, to string) int {
	panic("This is freeze state, only for initial")
}

func NewFreezeState(towerMap map[string]TowerInterface) StateInterface {
	return &FreezeState{
		&State{
			towerMap: towerMap,
		},
	}
}

type StepInterface interface {
	From() string
	To() string
	Disk() int
	String() string
}

type Step struct {
	from string
	to   string
	disk int
}

func (s *Step) From() string {
	return s.from
}

func (s *Step) To() string {
	return s.to
}

func (s *Step) Disk() int {
	return s.disk
}

func (s *Step) String() string {
	return fmt.Sprintf("move %d from %s to %s", s.disk, s.from, s.to)
}

type DeepStep struct {
	original StepInterface
	level    int
}

func (d *DeepStep) From() string {
	return d.original.From()
}

func (d *DeepStep) To() string {
	return d.original.To()
}

func (d *DeepStep) Disk() int {
	return d.original.Disk()
}

func (d *DeepStep) String() string {
	return fmt.Sprintf("%s by level %d", d.original.String(), d.level)
}

type ChangeStateActionInterface interface {
	From() StateInterface
	To() StateInterface
	Step() StepInterface
}

type ChangeStateAction struct {
	from StateInterface
	to   StateInterface
	step StepInterface
}

func (c *ChangeStateAction) From() StateInterface {
	return c.from
}

func (c *ChangeStateAction) To() StateInterface {
	return c.to
}

func (c *ChangeStateAction) Step() StepInterface {
	return c.step
}

type CommonSearch struct {
	begin       StateInterface
	end         StateInterface
	hashPastMap map[string]bool
}

func (s *CommonSearch) isEndState(state StateInterface) bool {
	return s.end.Hash() == state.Hash()
}

func (s *CommonSearch) getPossibleNextSteps(state StateInterface) []ChangeStateActionInterface {
	result := []ChangeStateActionInterface{}
	for _, from := range towerOrder {
		for _, to := range towerOrder {
			if from != to && state.CanMoveBetween(from, to) {
				nextState := state.Clone()

				disk := nextState.Move(from, to)

				if s.isUniqueState(nextState) {
					step := &Step{
						from: from,
						to:   to,
						disk: disk,
					}

					action := &ChangeStateAction{
						from: state,
						to:   nextState,
						step: step,
					}

					result = append(result, action)
				}
			}
		}
	}

	return result
}

func (s *CommonSearch) isUniqueState(state StateInterface) bool {
	if s.hashPastMap[state.Hash()] {
		return false
	}

	s.hashPastMap[state.Hash()] = true

	return true
}

func NewCommonSearch(begin, end StateInterface) *CommonSearch {
	return &CommonSearch{begin: begin, end: end, hashPastMap: map[string]bool{}}
}

type SearchInterface interface {
	Solve() bool
	StepHistory() []StepInterface
}

type BreadthFirstSearch struct {
	common       *CommonSearch
	stepHistory  []StepInterface
	hashStateMap map[string]ChangeStateActionInterface
}

func (s *BreadthFirstSearch) Solve() bool {
	result := s.queueSolve()

	if result {
		s.stepHistory = reverseStepHistory(s.unravel())
	}

	return result
}

func (s *BreadthFirstSearch) StepHistory() []StepInterface {
	return s.stepHistory
}

func (s *BreadthFirstSearch) queueSolve() bool {
	queue := []StateInterface{s.common.begin}
	s.common.isUniqueState(s.common.begin)

	for len(queue) > 0 {
		currentState := queue[len(queue)-1]
		queue = queue[:len(queue)-1]

		if s.common.isEndState(currentState) {
			return true
		}

		changes := s.common.getPossibleNextSteps(currentState)

		for _, changeStateAction := range changes {
			queue = append(queue, changeStateAction.To())

			s.hashStateMap[changeStateAction.To().Hash()] = changeStateAction
		}
	}

	return false
}

func (s *BreadthFirstSearch) unravel() []StepInterface {
	current := s.common.end.Hash()
	stepHistory := []StepInterface{}
	ok := true
	var changeStateAction ChangeStateActionInterface

	for ok {
		if changeStateAction, ok = s.hashStateMap[current]; ok {
			stepHistory = append(stepHistory, changeStateAction.Step())

			current = changeStateAction.From().Hash()
		}
	}

	return stepHistory
}

func NewBreadthFirstSearch(begin, end StateInterface) SearchInterface {
	return &BreadthFirstSearch{
		common:       NewCommonSearch(begin, end),
		hashStateMap: map[string]ChangeStateActionInterface{},
	}
}

type StatePriorityQueue struct {
	values map[float64]StateInterface
	size   int
	shift  float64
}

func (pq *StatePriorityQueue) Add(weight float64, value StateInterface) {
	pq.values[pq.freeWeight(weight)] = value
	pq.size++
}

func (pq *StatePriorityQueue) Next() StateInterface {
	min := math.MaxFloat64

	for key := range pq.values {
		min = math.Min(min, key)
	}

	value := pq.values[min]

	delete(pq.values, min)

	pq.size--

	return value
}

func (pq *StatePriorityQueue) freeWeight(weight float64) float64 {
	pq.shift += 0.000001

	for {
		exists := pq.values[weight]

		if exists == nil {
			return weight
		}

		weight += pq.shift
	}

	return weight
}

func (pq *StatePriorityQueue) Len() int {
	return pq.size
}

func NewStatePriorityQueue() *StatePriorityQueue {
	return &StatePriorityQueue{values: map[float64]StateInterface{}}
}

type BestFirstSearch struct {
	*BreadthFirstSearch
}

func (s *BestFirstSearch) Solve() bool {
	result := s.queueSolve()

	if result {
		s.stepHistory = reverseStepHistory(s.unravel())
	}

	return result
}

func (s *BestFirstSearch) queueSolve() bool {
	priotityQueue := NewStatePriorityQueue()
	priotityQueue.Add(0, s.common.begin)
	s.common.isUniqueState(s.common.begin)

	for priotityQueue.Len() > 0 {
		currentState := priotityQueue.Next()

		if s.common.isEndState(currentState) {
			return true
		}

		changes := s.common.getPossibleNextSteps(currentState)

		for _, changeStateAction := range changes {
			to := changeStateAction.To()

			priotityQueue.Add(s.stateDiff(to), to)

			s.hashStateMap[to.Hash()] = changeStateAction
		}
	}

	return false
}

func (s *BestFirstSearch) stateDiff(state StateInterface) float64 {
	var result float64

	endTowerMap := s.common.end.TowerMap()

	for key, tower := range state.TowerMap() {
		result += s.towerDiff(tower, endTowerMap[key])
	}

	return result
}

func (s *BestFirstSearch) towerDiff(left, right TowerInterface) float64 {
	leftLength := float64(left.Len())
	rightLength := float64(right.Len())

	return math.Abs(math.Pow(rightLength, 2) - math.Pow(leftLength, 2))
}

func NewBestFirstSearch(begin, end StateInterface) SearchInterface {
	return &BestFirstSearch{&BreadthFirstSearch{
		common:       NewCommonSearch(begin, end),
		hashStateMap: map[string]ChangeStateActionInterface{},
	}}
}

type DeepFirstSearch struct {
	common      *CommonSearch
	stepHistory []StepInterface
}

func (s *DeepFirstSearch) Solve() bool {
	result := s.deep(s.common.begin, 0)

	if result {
		s.stepHistory = reverseStepHistory(s.stepHistory)
	}

	return result
}

func (s *DeepFirstSearch) StepHistory() []StepInterface {
	return s.stepHistory
}

func (s *DeepFirstSearch) deep(state StateInterface, level int) bool {
	if s.common.isEndState(state) {
		return true
	}

	changes := s.common.getPossibleNextSteps(state)

	for _, changeStateAction := range changes {
		if s.deep(changeStateAction.To(), level+1) {
			step := changeStateAction.Step()

			s.stepHistory = append(s.stepHistory, &DeepStep{original: step, level: level})

			return true
		}
	}

	return false
}

func NewDeepFirstSearch(begin, end StateInterface) SearchInterface {
	return &DeepFirstSearch{common: NewCommonSearch(begin, end)}
}

func StoreStepHistory(fileName string, stepHistory []StepInterface) {
	file, _ := os.Create(fileName)
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, step := range stepHistory {
		w.WriteString(step.String() + "\n")
	}
	w.Flush()
}

func reverseStepHistory(stepHistory []StepInterface) []StepInterface {
	length := len(stepHistory)
	result := make([]StepInterface, 0, length)

	for index := length - 1; index > -1; index-- {
		result = append(result, stepHistory[index])
	}

	return result
}

func main() {
	tower := NewTower([]int{1, 2, 3})
	fmt.Println("Tower of Hanoi", tower)
}
